package com.hsbc.storeapp.model;

public enum ItemType {

	FOOD_ITEM,
	APPAREL,
	ELECTRONIC
}
