package com.hsbc.grad1.list;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ArrayListDemo {
	
	public static void main(String[] args) {
		List<Integer> numbers = new LinkedList<>();
		
		System.out.printf("Initial size of the numbers %d %n", numbers.size());
		numbers.add(34);
		numbers.add(44);
		numbers.add(54);
		numbers.add(64);
		numbers.add(65);
		System.out.printf("Final size of the numbers %d %n", numbers.size());
		numbers.remove(0);
		numbers.remove(0);
		numbers.remove(0);
		numbers.remove(0);
		System.out.printf("Final size of the numbers %d after deletion %n", numbers.size());
		
		numbers.clear();
		numbers.contains(45);
		
		//numbers.get(3);
		numbers.isEmpty();
		
	}

}
