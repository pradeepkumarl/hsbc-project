package com.hsbc.storeapp.service;

import java.util.Set;

import com.hsbc.storeapp.dao.ItemDAO;
import com.hsbc.storeapp.exception.ItemNotFoundException;
import com.hsbc.storeapp.model.Item;
import com.hsbc.storeapp.model.ItemType;

public interface ItemService {
	
	
	Item saveItem(Item item);
	
	Set<Item> fetchItems(ItemType itemType);
	
	Item fetchItemsByItemCode(String ItemCode) throws ItemNotFoundException;

	Set<Item> fetchItemsSortedByMfgDate();

}
