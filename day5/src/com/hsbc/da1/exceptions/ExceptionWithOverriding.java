package com.hsbc.da1.exceptions;

import java.io.FileNotFoundException;
import java.io.IOException;

public class ExceptionWithOverriding {

}

class Super{
	
	public void method() throws IOException, ArithmeticException{
		
	}
	
}

class Sub extends Super {
	
	/*
	 * Overriding rules
	 * 1. The sub class method need not throw any exception declared
	 * in the parent class
	 * 2. The sub class method need not declare any UncheckedException 
	 * in the throws clause
	 * 3. The sub class method can only declare a subset of checked 
	 * exception declared in the super class method
	 */
	public void method() throws FileNotFoundException {
		
	}
	
}
