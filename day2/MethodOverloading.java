public class MethodOverloading {

    public static void main(String[] args) {
        MethodOverloading obj = new MethodOverloading();

        int i = 44;
        long l = 44;

        obj.concat("Hello", "World");
        obj.concat(i, l);
        obj.concat(44, 66, 88);

        System.out.println(i);
        System.out.println(l);
        System.out.println("String");
        System.out.println(false);
        System.out.println();

    }

    public int concat(int a, long b) {
        return (int) (a + b);
    }

    public int concat(long a, int b) {
        return (int) (a + b);
    }

    public int concat(int a, int b, int c) {
        return a + b + c;
    }

    public String concat(String a, String b) {
        return a + b;
    }
}