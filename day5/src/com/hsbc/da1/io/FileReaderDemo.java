package com.hsbc.da1.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Date;

public class FileReaderDemo {
	public static void main(String[] args) {
		File textFile = new File("D://test.txt");
//		System.out.printf("Absolute Path %s %n" ,textFile.getAbsolutePath());
//		System.out.printf("Is a directory %s %n" ,textFile.isDirectory());
//		System.out.printf("Is a file %s %n" ,textFile.isFile());	

		// read the contents from the file
//		readFile(textFile, "D://destination.txt");
		readObject();
		
	}
	
	private static void readObject() {
		// TODO Auto-generated method stub
		try {
			boolean endOfFile = false;
			 FileInputStream fos = new FileInputStream("D://t.txt");
			 ObjectInputStream oos = new ObjectInputStream(fos);
			 System.out.println(oos.read());
			 oos.close();

			 
		}catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void readFile(File textFile, String destFile) {
		try (
				BufferedReader bufferedReader = new BufferedReader (new FileReader(textFile));
				BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(destFile)));
				
			) {
			boolean endOfFile = false;
			 FileOutputStream fos = new FileOutputStream("D://t.txt");
		      ObjectOutputStream oos = new ObjectOutputStream(fos);
		      oos.writeInt(12345);
		      oos.writeObject("Today");
		      oos.writeObject(new Date());

		      oos.close();

			while (!endOfFile) {
				String line = bufferedReader.readLine();
				if ( line != null) {
					bufferedWriter.append(line);
					bufferedWriter.newLine();
					
				} else {
					endOfFile = true;
				}

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
