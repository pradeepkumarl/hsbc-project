public class DataTypes {

    // primitive data types
    public static void main(String args[]) {
        /*
         * byte short integer long
         * 
         * float double
         * 
         * boolean
         * 
         * char
         * 
         */

        // assign a data type to a variable
        /*
         * 1. Declaration 2. Assignment
         */

        // declaration
        int value;

        // assignment
        value = 56;

        // inline assignment of data to a variable
        // data-type variable_name = value;
        short noOfDaysInAYear = 365;

        byte noOfDaysInAMonth = 30;

        int noOfEmployees = 45_000;

        double distanceFromEarthToSun = 45_00_000.9900;

        boolean isMarried = false;

        char aChar = 'A';

        String name = "Suraj";

        System.out.println("The byte value is " + value);
        System.out.println("The name is " + name);
    }
}