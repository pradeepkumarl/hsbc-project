<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.*,com.hsbc.da1.model.Item" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="hsbc" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Listing all the Items</title>
<style>
  table {
    border: 1px solid red;
  }
  td, tr {
    border: 1px solid red;
  }
</style>
</head>
<body>
<%@ include file="header.jsp" %>

       <%
         List<Item> items = (List)request.getAttribute("items");
         request.setAttribute("cart", Arrays.asList(new Item(100, "pen", 200), new Item(102, "mouse", 850)));
         request.setAttribute("carts", null);
        %>

  <table>
    <thead>
      <tr>
       <th>Id</th>
       <th>Name</th>
       <th>Price</th>
      </tr> 
    </thead>
    <tbody>
     <hsbc:forEach items="${cart}" var="c">
            <tr>
             <td><hsbc:out value="${c.getId()}" /></td>
             <td><hsbc:out value="${c.getName()}" /></td>
             <td><hsbc:out value="${c.getPrice()}" /></td>
            </tr> 
     </hsbc:forEach> 
    </tbody>
  </table>


  <hsbc:choose>
   <hsbc:when test="${cart == null}"> Cart is empty </hsbc:when>
   <hsbc:otherwise> Cart is not empty </hsbc:otherwise>
  </hsbc:choose>  
  
</body>
</html>