abstract class BankAccount {
    public String name;
    private double balance;
    private final long accountNumber;
    private static long counter = 1000;

    public BankAccount(String name, double balance) {
        this.name = name;
        this.balance = balance;
        this.accountNumber = ++counter;
    }

    public final void deposit(double amount) {
        this.balance += amount;
    }

    public final String getCustomerDetails() {
        return "Customer name: " + this.name + " Balance: " + this.balance + "A/C No: " + this.accountNumber;
    }

    public abstract double withdraw(double amount);

    public abstract double getLoanAmount(double loanAmount);

    public abstract double loanElibibilty();

    public static void method1() {
        System.out.println("Inside the parent class");
    }
}

final class CurrentAccount extends BankAccount {
    private String gstNumber;
    private String name;
    private final long LOAN_ELIGIBILITY = 25_00_000;

    public CurrentAccount(double balance, String name, String gstNumber) {
        super(name, balance);
        this.gstNumber = gstNumber;
        this.name = name;
        super.name = name;
    }

    public final double withdraw(double amount) {

        return amount;
    }

    public final double getLoanAmount(double loanAmount) {
        // validate the loan eligibility
        super.deposit(loanAmount);
        return loanAmount;
    }

    public final double loanElibibilty() {
        return LOAN_ELIGIBILITY;
    }

    public static void method1() {
        System.out.println("Inside the parent class");
    }
}

public final class BankAccountClient {

    public static void main(String[] args) {
        String value = args[0];
        BankAccount account;

        switch (value) {
            case "0":
                account = new CurrentAccount(10000, "", "sdfsdds");
                break;
            default:
                account = new CurrentAccount(10000, " ", "sdfsdds");
                break;
        }

        account.getCustomerDetails();
        account.withdraw(2000);
    }

}