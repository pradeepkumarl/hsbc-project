package com.hsbc.da1.model;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SavingsAccountDemo {
	
	public static void main(String[] args) {
		
		SavingsAccount sa = new SavingsAccount("dinesh", 35000, "dinesh@gmail.com");

		
		Set<SavingsAccount> savingsAccountSet = new HashSet<>();
		
		savingsAccountSet.add(sa);
		
	
		
		Iterator<SavingsAccount> it = savingsAccountSet.iterator();
		
		while(it.hasNext()) {
			System.out.println(it.next());
		}
		
	}
}
