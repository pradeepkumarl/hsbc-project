<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<h1>Setting Cookies !!</h1>

<%
   Cookie cookie1 = new Cookie("firstname", "pradeep");
   Cookie cookie2 = new Cookie("lastname", "kumar");
   response.addCookie(cookie1);
   response.addCookie(cookie2);
%>

<h2>Listing all the cookies </h2>
<%
    Cookie[] cookies = request.getCookies();

    for ( Cookie cookie: cookies){
    	%>
    	<span> Name: <%= cookie.getName()  %> </span> <span>Value: <%= cookie.getValue() %></span>
    	<%
    }
%>

</body>
</html>