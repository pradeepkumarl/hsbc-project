
public class Method {

    static int initialValue = 10;

    public static void main(String[] args) {

        int row = 4;
        int col = 4;
        int[][] matrix = new int[row][col];

        assignMatrix(matrix);
        printMatrix(matrix);

    }

    public static void assignMatrix(int[][] matrix) {
        for (int rowIndex = 0; rowIndex < 4; rowIndex++) {
            for (int colIndex = 0; colIndex < 4; colIndex++) {
                matrix[rowIndex][colIndex] = initialValue++;
            }
        }
    }

    public static void printMatrix(int[][] matrix) {
        for (int rowIndex = 0; rowIndex < 4; rowIndex++) {
            for (int colIndex = 0; colIndex < 4; colIndex++) {
                System.out.print("\t" + matrix[rowIndex][colIndex] + " \t");
            }
            System.out.println();
        }
    }
}