package com.hsbc.storeapp.model;

public class Apparel extends Item {
	

public enum Type {
	WOOL,
	COTTON
}
	
	private int size;
	private Type type;

	public Apparel(String itemCode, String itemName, int size, Type type) {
		super(itemCode, itemName);
		this.size = size;
		this.type = type;
	}

	/**
	 * @return the size
	 */
	public int getSize() {
		return size;
	}



	/**
	 * @param size the size to set
	 */
	public void setSize(int size) {
		this.size = size;
	}



	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}



	/**
	 * @param type the type to set
	 */
	public void setType(Type type) {
		this.type = type;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + size;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof Apparel))
			return false;
		Apparel other = (Apparel) obj;
		if (size != other.size)
			return false;
		if (type != other.type)
			return false;
		return true;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Apparel [size=" + size + ", type=" + type + ", getItemCode()=" + getItemCode() + ", getItemName()="
				+ getItemName() + ", getQty()=" + getQty() + ", getPrice()=" + getPrice() + ", toString()="
				+ super.toString() + ", hashCode()=" + hashCode() + ", getClass()=" + getClass() + "]";
	}
	
	

}

