public class CallHirearchy {

    public static void main(String[] args) {
        method1();
        System.out.println("Finally getting invoked in the main method");
    }

    private static void method1() {
        method2();
        System.out.println("Called inside method 1 after invoking method2");
    }

    private static void method2() {
        method3();
        System.out.println("Called inside method 2 after invoking method3");
    }

    private static void method3() {
        System.out.println("Called inside method 3");
    }

}