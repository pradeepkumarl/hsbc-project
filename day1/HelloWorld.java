/**
 * This class demonstrate the Hello World
 * 
 * @since 1.0
 * @author pradeep
 * @version 1.0
 */
public class HelloWorld {
    public static void main(String args[]) {
        // inline comment
        // declaring variable
        System.out.println("Welcome to HSBC Grads Program !!");

        /*
         * The below block of code computes the random distribution of erros caused
         * during an outage
         */
    }
}