
public class SavingsAccountRegister {

    private static SavingsAccount[] savingsAccounts = new SavingsAccount[] {
            new SavingsAccount("Naveen", 50_000, "Disha"), new SavingsAccount("Rajeesh", 10_000),
            new SavingsAccount("Vinay", 34_000, new Address("8th Ave", "Bangalore", "Karnataka", 577142))

    };

    public static SavingsAccount fetchSavingsAccountByAccountId(long accountId) {

        for (SavingsAccount savingsAccount : savingsAccounts) {
            if (savingsAccount.getAccountNumber() == accountId) {
                return savingsAccount;
            }
        }
        return null;
    }
}