public class Constructs {

    public static void main(String[] args) {
        boolean flag = false;
        // if block will be executed based on evaluating a boolean expression
        if (flag) {
            System.out.println("The value is true ");
        } else {
            System.out.println("The value is false ");
        }

        String day = "SUNDAY";

        // switch
        switch (day) {
            case "SUNDAY":
                System.out.println(" Its a Weekend !!");
                break;
            case "SATURDAY":
                System.out.println(" Its a Weekend !!");
                break;
            default:
                System.out.println(" Its a Weekday !!");
                break;
        }

        //ternary operator
        // variable name = expression ? true : false;
        String result = ( day == "SUNDAY" || day == "SATURDAY" ) ?  "Weekend": "Weekday";
    }
}