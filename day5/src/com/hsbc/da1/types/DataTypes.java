package com.hsbc.da1.types;
import static java.lang.Integer.*;

public class DataTypes {
	
	public static void main(String[] args) {
		int i = 56;
		//classes called wrapper classes
		/*
		 * Integer
		 * Boolean
		 * Character
		 * Byte
		 * Short
		 * Double
		 * Long
		 * Float
		 */
		
		String s = "hello";
		
		System.out.println(s.toUpperCase().toLowerCase());
		
		Integer number = Integer.parseInt("45");
		boolean b = false;
		
		int test = 45;
		Integer objTest = test;
		test = objTest;
		
		System.out.printf("Larger number of %d %d is %d ", 34, 56, min(34, 56) );
		
		
	}
	

}
