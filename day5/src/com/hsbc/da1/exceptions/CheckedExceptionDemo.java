package com.hsbc.da1.exceptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class CheckedExceptionDemo {
	
	public static void main(String[] args) {
		
		String value = args[0];
		if ( value.equals("0")) {
			try {
				readContentsOfTheFile("D://test.txt");
			} catch (FileNotFoundException e) {
				System.out.println("File is not present.");
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			try {
				readContentsOfTheFile("D://test.txt");
			} catch (FileNotFoundException e) {
				System.out.println("Please enter a valid file name");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static void readContentsOfTheFile(String file) throws FileNotFoundException, IOException {
			FileReader reader = new FileReader(new File(file));
			System.out.println((char)reader.read());
	}
}
