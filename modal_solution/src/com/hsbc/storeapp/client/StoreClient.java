package com.hsbc.storeapp.client;

import java.time.LocalDate;
import java.util.Scanner;

import com.hsbc.storeapp.model.Apparel;
import com.hsbc.storeapp.model.FoodItem;
import com.hsbc.storeapp.model.Item;
import com.hsbc.storeapp.model.ItemType;
import com.hsbc.storeapp.service.ItemService;
import com.hsbc.storeapp.service.ItemServiceImpl;

public class StoreClient {
	
	public static void main(String[] args) {
		
		Item breads = new FoodItem("ITF-001", "Breads", LocalDate.of(2020, 10, 12), LocalDate.of(2020, 11, 12));
		Item jeans = new Apparel("ITA-001", "Levis-Jeans", 34, Apparel.Type.COTTON);
		
		ItemService itemService = new ItemServiceImpl();
		
		Scanner sc = new Scanner(System.in);
		
	System.out.println("Please enter the options");
	
	    System.out.println(" Enter 1 for save ");
	    System.out.println(" Enter 2 for fetch ");
	    
	    int option = sc.nextInt();
	    switch (option) {
		case 1:
			itemService.saveItem(breads);
			break;
		case 2:
			itemService.fetchItems(ItemType.FOOD_ITEM);
			break;
			
		case 3:
			itemService.fetchItemsSortedByMfgDate();
			break;
		case 4:
			itemService.fetchItemsSortedByMfgDate();
			break;
		default:
			break;
		}

	    sc.close();
		
	}

}
