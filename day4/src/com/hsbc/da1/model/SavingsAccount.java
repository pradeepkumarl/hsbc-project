package com.hsbc.da1.model;

import java.io.Serializable;

public class SavingsAccount implements Comparable<SavingsAccount>, Serializable {

	private static final long serialVersionUID = 1L;

	private String customerName;
	
	private long accountNumber;
	
	private double accountBalance;
	
	private Address address;

	private final String emailAddress;
	
	
	public SavingsAccount(String customerName, double accountBalance, String emailAddress) {
		this.customerName = customerName;
		this.accountBalance = accountBalance;
		this.emailAddress = emailAddress;
	}
	
	public SavingsAccount(long accountNumber, String customerName, double accountBalance, String emailAddress) {
		this.customerName = customerName;
		this.accountBalance = accountBalance;
		this.emailAddress = emailAddress;
		this.accountNumber = accountNumber;
	}

	
	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public double getAccountBalance() {
		return this.accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	public long getAccountNumber() {
		return accountNumber;
	}


	@Override
	public int compareTo(SavingsAccount account) {
	 	 //return (int)(this.accountBalance - account.getAccountBalance());
		return -1 * account.customerName.compareTo(this.customerName);
	}



	public String getEmailAddress() {
		return this.emailAddress;
	}

	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (accountNumber ^ (accountNumber >>> 32));
		result = prime * result + ((customerName == null) ? 0 : customerName.hashCode());
		result = prime * result + ((emailAddress == null) ? 0 : emailAddress.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof SavingsAccount))
			return false;
		SavingsAccount other = (SavingsAccount) obj;
		if (accountNumber != other.accountNumber)
			return false;
		if (customerName == null) {
			if (other.customerName != null)
				return false;
		} else if (!customerName.equals(other.customerName))
			return false;
		if (emailAddress == null) {
			if (other.emailAddress != null)
				return false;
		} else if (!emailAddress.equals(other.emailAddress))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SavingsAccount [customerName=" + customerName + ", accountNumber=" + accountNumber + ", accountBalance="
				+ accountBalance + ", address=" + address + ", emailAddress=" + emailAddress + "]";
	}
	
	
}
