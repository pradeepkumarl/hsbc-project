package com.hsbc.da1.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Date;
import java.util.Scanner;

public class DateDemo {
	
	public static void main(String[] args) {
		
		
		//java.util.Date
		// java.util.Calendar 
		
		Scanner sc = new Scanner(System.in);
		System.out.println(" Input the date in dd-mm-yyyy format");
		String input = sc.next();
		
		LocalDate dob = LocalDate.parse(input, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
		
		System.out.println("Date "+ dob.getDayOfMonth());
		System.out.println("Month "+ dob.getMonthValue());
		System.out.println("Year "+ dob.getYear());
		
		LocalDate currentDate = LocalDate.now();
		LocalDate futureDate = LocalDate.of(2020, 9, 28);
		
		System.out.println(Period.between(currentDate, futureDate).getDays());;
//		LocalDate futureDate = LocalDate.of(2021, 4, 15);
		

		
		
	//	currentDate.isBefore(futureDate);
		

		System.out.println("Coverting from string to date ");
//		LocalDate parsedDate = LocalDate.parse(input, DateTimeFormatter.ofPattern("dd-MM-yy"));
		
	//	System.out.println("Day of the Month " +parsedDate.getDayOfMonth());
		//System.out.println("Day of the year" +parsedDate.getDayOfYear());
		
		calculateDateDiffUsingDate();
	}
	
	@Deprecated
	private static void calculateDateDiffUsingDate() {
		
	}
	

	private static void calculateDateDiffUsingLocalDate() {
		
	}

}
