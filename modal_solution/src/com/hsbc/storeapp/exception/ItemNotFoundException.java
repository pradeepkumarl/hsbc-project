package com.hsbc.storeapp.exception;

public class ItemNotFoundException extends Exception{
	
	public ItemNotFoundException(String message) {
		super(message);
	}
	
	@Override
	public String getMessage() {
		return super.getMessage();
	}
	

}
