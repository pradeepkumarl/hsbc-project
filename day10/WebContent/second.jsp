<%@ page language="java" 
         contentType="text/html; charset=ISO-8859-1"
         errorPage="error.jsp"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%@ include file="header.jsp" %>
<h1> Welcome to Second JSP file!!  </h1>

<%= ((String)request.getAttribute("requestScope")).toUpperCase() %>
<%= session.getAttribute("sessionScope") %>
<%= application.getAttribute("app") %>

</body>
</html>