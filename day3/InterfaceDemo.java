interface A {

    public void method();
}

interface B {
    public void method();
}

public class InterfaceDemo implements A, B {
    public void method() {
        System.out.println("Implemente with InterfaceDemo");
    }

    public void internalMethod() {
        System.out.println("Internal method");
    }

    public static void main(String[] args) {
        A a = new InterfaceDemo();
        B b = new InterfaceDemo();
        InterfaceDemo c = new InterfaceDemo();

        b.method();
    }
}