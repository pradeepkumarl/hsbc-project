package com.hsbc.da1.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcDemo {
    private static final String INSERT_QUERY = "insert into items (name, price)"
            + " values ('iPhone', 22000), ('iPad', 30000), ('macbookPro', 150000)";
    
    private static final String SELECT_QUERY = "select * from items";
    
    private static String connectString = "jdbc:derby://localhost:1527/items ";
    private static String username = "admin";
    private static String password = "password";

    public static void main(String[] args) {

        try (Connection connection = DriverManager
                            .getConnection(connectString, username, password); 
                Statement statement = connection.createStatement();) {
          //  int updatedRows = statement.executeUpdate(INSERT_QUERY);
            
            //System.out.println(" Number of records updated " + updatedRows);
            
            ResultSet resultSet = statement.executeQuery(SELECT_QUERY);
            while (resultSet.next()) {
                String name = resultSet.getString(1);
                double price = resultSet.getDouble(2);
                
                System.out.printf( "Name:  %s,  Price: %s %n", name, price);
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}