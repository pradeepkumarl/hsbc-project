package com.hsbc.storeapp.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Set;

import com.hsbc.storeapp.dao.ItemDAO;
import com.hsbc.storeapp.exception.ItemNotFoundException;
import com.hsbc.storeapp.model.FoodItem;
import com.hsbc.storeapp.model.Item;
import com.hsbc.storeapp.model.ItemType;

public class ItemServiceImpl implements ItemService {

	ItemDAO itemDAO = ItemDAO.getItemDAO();

	@Override
	public Item saveItem(Item item) {
		// TODO Auto-generated method stub
		return this.itemDAO.saveItem(item);
	}

	@Override
	public Set<Item> fetchItems(ItemType itemType) {
		// TODO Auto-generated method stub
		return this.itemDAO.fetchItems(itemType);
	}

	@Override
	public Item fetchItemsByItemCode(String itemCode) throws ItemNotFoundException {
		return this.itemDAO.fetchItemByItemCode(itemCode);
	}

	@Override
	public Set<Item> fetchItemsSortedByMfgDate() {
	
		Set<Item> allItems = this.itemDAO.fetchItems(ItemType.FOOD_ITEM);
		Collections.sort(new ArrayList<Item>(allItems), new SortItemByMfgDate());
		return null;
	}
}

class SortItemByMfgDate implements Comparator<Item>{

	@Override
	public int compare(Item one, Item two) {

		FoodItem f1 = (FoodItem)one;
		FoodItem f2 = (FoodItem)two;
		return f1.getManfDate().compareTo(f2.getManfDate());
	}
	
}
