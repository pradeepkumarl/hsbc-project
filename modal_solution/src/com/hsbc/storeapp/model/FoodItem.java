package com.hsbc.storeapp.model;

import java.time.LocalDate;

public class FoodItem extends Item {

	private LocalDate manfDate;
	
	private LocalDate expDate;
	
	private boolean isVeg;
	
	private static int itemSold = 1000;
	
	
	public FoodItem(String itemCode, String itemName, LocalDate manfDate, LocalDate expDate ) {
		super(itemCode, itemName);
		this.manfDate = manfDate;
		this.expDate = expDate;
		-- itemSold;
	}


	/**
	 * @return the manfDate
	 */
	public LocalDate getManfDate() {
		return manfDate;
	}


	/**
	 * @param manfDate the manfDate to set
	 */
	public void setManfDate(LocalDate manfDate) {
		this.manfDate = manfDate;
	}


	/**
	 * @return the expDate
	 */
	public LocalDate getExpDate() {
		return expDate;
	}


	/**
	 * @param expDate the expDate to set
	 */
	public void setExpDate(LocalDate expDate) {
		this.expDate = expDate;
	}


	/**
	 * @return the isVeg
	 */
	public boolean isVeg() {
		return isVeg;
	}


	/**
	 * @param isVeg the isVeg to set
	 */
	public void setVeg(boolean isVeg) {
		this.isVeg = isVeg;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((expDate == null) ? 0 : expDate.hashCode());
		result = prime * result + (isVeg ? 1231 : 1237);
		result = prime * result + ((manfDate == null) ? 0 : manfDate.hashCode());
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof FoodItem))
			return false;
		FoodItem other = (FoodItem) obj;
		if (expDate == null) {
			if (other.expDate != null)
				return false;
		} else if (!expDate.equals(other.expDate))
			return false;
		if (isVeg != other.isVeg)
			return false;
		if (manfDate == null) {
			if (other.manfDate != null)
				return false;
		} else if (!manfDate.equals(other.manfDate))
			return false;
		return true;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FoodItem [manfDate=" + manfDate + ", expDate=" + expDate + ", isVeg=" + isVeg + "]";
	}
	
	
	
	

}
