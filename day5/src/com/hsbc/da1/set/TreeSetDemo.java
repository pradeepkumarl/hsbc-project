package com.hsbc.da1.set;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class TreeSetDemo {
	
	public static void main(String[] args) {
		Set<Integer> integers = new TreeSet<>();
		
		integers.add(34);
		integers.add(45);
		integers.add(34);
		integers.add(66);
		integers.add(35);
		integers.add(34);
		integers.add(36);
		integers.add(45);
		integers.add(34);
		integers.add(66);
		
		Iterator<Integer> it = integers.iterator();
		
		while (it.hasNext()) {
			System.out.println(it.next());
		}
	}

}
