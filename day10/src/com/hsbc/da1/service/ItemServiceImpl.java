package com.hsbc.da1.service;

import java.util.List;

import com.hsbc.da1.dao.ItemDAO;
import com.hsbc.da1.model.Item;

public class ItemServiceImpl implements ItemService {
	
	private ItemDAO itemDAO = ItemDAO.getInstance();

	@Override
	public Item saveItem(Item item) {
		// TODO Auto-generated method stub
		return this.itemDAO.saveItem(item);
	}

	@Override
	public List<Item> fetchItems() {
		// TODO Auto-generated method stub
		return itemDAO.fetchItems();
	}

}
