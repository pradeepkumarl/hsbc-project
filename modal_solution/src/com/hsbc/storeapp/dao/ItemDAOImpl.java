package com.hsbc.storeapp.dao;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.hsbc.storeapp.exception.ItemNotFoundException;
import com.hsbc.storeapp.model.Item;
import com.hsbc.storeapp.model.ItemType;

public class ItemDAOImpl implements ItemDAO {
	
	private Map<ItemType, Set<Item>> items = new HashMap<>();
	private Set<Item> itemsSet = new HashSet<>();

	@Override
	public Item saveItem(Item item) {
		itemsSet.add(item);
		items.put(item.getItemType(), itemsSet);
		return item;
	}

	@Override
	public Set<Item> fetchItems(ItemType itemType) {
		return this.items.get(itemType);
	}

	@Override
	public Item fetchItemByItemCode(String itemCode)throws ItemNotFoundException {
		throw new ItemNotFoundException("Item with given itemCode "+itemCode+" is not present");
	}

}
