<%@ page language="java" 
         contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" 
         import="java.util.Date"%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>My First JSP file</title>
</head>
<body>

<!--  Used to include static content -->
<%@ include file="header.jsp" %>

<!--  Used to include dynamic content -->
<jsp:include page="header.jsp" />

 
 
 <%-- This is a comment inside a JSP --%>
 <!--  THis is a HTML comment -->
 <%-- This is called a scriplet --%>
 
<% 
          
          int i  = 56;
          int j = 56;
          out.write("The sum is "+ (i + j));
          out.write(new Date().toString()); 
          test();
%>

<%-- This is called a declaration --%>

<%! public void test() {
	   System.out.println("THis is a regular Java method");
    }
%>

<br/>

<span>Current date is : <%= new java.util.Date().toString() %> in ISO format </span>      

<%
     request.setAttribute("requestScope", "Request");
     session.setAttribute("sessionScope", "Session");
     application.setAttribute("app", "application-scope");
%>


<a href="second.jsp">Click here to navigate to Second jsp file</a>
       
</h2>
</body>
</html>