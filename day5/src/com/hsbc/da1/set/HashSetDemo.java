package com.hsbc.da1.set;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class HashSetDemo {
	
	public static void main(String[] args) {
		Set<Integer> set = new HashSet<>();
		set.add(23);
		set.add(23);
		set.add(23);
		set.add(23);
		set.add(23);
		set.add(33);
		set.add(44);
		set.add(55);
		
		int array[] = {435,45,66,77, 88};
		

		System.out.println(array);
		
		System.out.printf("Total number of elements %d %n", set.size());
		System.out.println(set.contains(23));
		
		//Iterator
		Iterator<Integer> it = set.iterator();
		
		while(it.hasNext()) {
			int value = it.next();
		//	System.out.println("The value is "+ value);
		}
		
		//System.out.println("Size of the set : " +set.size());
		
	}

}
