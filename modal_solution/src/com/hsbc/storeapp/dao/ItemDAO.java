package com.hsbc.storeapp.dao;

import java.util.Set;

import com.hsbc.storeapp.exception.ItemNotFoundException;
import com.hsbc.storeapp.model.Item;
import com.hsbc.storeapp.model.ItemType;

public interface ItemDAO {
	
	static final ItemDAO itemDAO = new ItemDAOImpl();
	
	Item saveItem(Item item);
	
	Set<Item> fetchItems(ItemType itemType);
	
	static ItemDAO getItemDAO() {
		return itemDAO;
	}

	Item fetchItemByItemCode(String itemCode) throws ItemNotFoundException;

}
